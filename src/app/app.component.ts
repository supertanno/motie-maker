import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {FormsModule} from "@angular/forms";
import {NgForOf, NgIf} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FormsModule, NgForOf, NgIf],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  subject = ""
  indiener = ""
  meeting = ""
  date = new Date()
  constateringen: string[] = [""]
  overwegingen: string[] = [""]
  wie = ""
  actions: string[] = [""]

}
